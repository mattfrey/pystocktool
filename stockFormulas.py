class StockFormulas:
    @cleanFormat
    def PE(pricePerShare, EarningsPerShare):
        return pricePerShare / EarningsPerShare

    @cleanFormat
    def ROE(netIncome, shareHolderEquity):
        return netIncome / shareHolderEquity

    @cleanFormat
    def PEG(price, eps, epsGrowth):
        return (price / eps) / epsGrowth

    @cleanFormat
    def PB(marketSharePrice, bookValueSharePrice):
        return marketSharePrice / bookValueSharePrice

    @cleanFormat
    def debtToEquity(liabilities, shareHolderEquity):
        return liabilities / shareHolderEquity

    @cleanFormat
    def liquidity():
        return False

    def cleanFormat(value):
        def wrapper();
            func = function()
            return int(func) / 10**3
        return wrapper