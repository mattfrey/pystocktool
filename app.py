from flask import Flask, make_response
from finviz.screener import Screener
import finviz
import requests as r
import Config.Config as Config
# from routes.Route import Route

app = Flask(__name__)
# Route(app)

api, secret = Config().values()
iex_api = 'https://cloud.iexapis.com/v1/'
token = f'&token={api}'

# class Route(app):
  # === Basic Routes === 
@app.route('/dashboard', methods=['GET'])
def dashboard():
  return "Dashboard"

@app.route('/predefined', methods=['GET'])
def predefined_screener():
  predefined_scanners = []
  return "Predefined"

@app.route('/predefined/screen', methods=['POST'])
def predefined_screener_call():
  results = []
  return "Predefined Screener"

@app.route('/custom', methods=['GET'])
def custom_screener():
  form_fields = []
  return "Custom Form"

@app.route('/custom/screen', methods=['POST'])
def custom_screener_call():
  data = request.get_json()
  results = []

  return {'results':data}

@app.route('/ticker/<ticker>', methods=['GET'])
def get_ticker(ticker):
  response = {
    'message': '',
    'data': None,
    'news': None,
    'chart': None,
    'error': None
  }
  try:  
    chart_data = r.get(f'''{iex_api}/stock/{ticker}/batch?types=chart&range=1m&last=10{token}''')
    if chart_data.status_code == 200:
      response['chart'] = chart_data.json()
    else:
      response['chart'] = None
    response['data'] = finviz.get_stock(ticker)
    response['news'] = list(map(
      lambda tick: { "title": tick[0], "href": tick[1] }, 
      finviz.get_news(ticker)
    ))
    if response['data']:
      response['message'] = f'''Stock Data was found for {ticker} stock.'''
    else:
      response['message'] = f'''Couldn't find any stock data for {ticker}...'''
  except Exception as e:
    response['errors'] = f'''{e}'''
  
  return make_response(response)

@app.route('/ticker/<path:country>/<ticker>', methods=['GET'])
def get_ticker_by_country(country, ticker):
  #can use for CA tickers
  return f"Ticker: {ticker}"

@app.route('/news', methods=['GET'])
def all_news():
  news = finviz.get_all_news()
  return "All News"

# === incorporate users ===

# @app.route('/dashboard/<user>/profile', methods=['GET'])
# def profile(user):
#   return "Profile"

# @app.route('/dashboard/<user>/my-list', methods=['GET'])
# def my_list(user):
#   return "List"

# @app.route('/dashboard/<user>/messages', methods=['GET'])
# def messages(user):
#   return "Messages"

# @app.route('/dashboard/learn/terms', methods=['GET'])
# def Terms():
#   return "Terms"

# @app.route('/dashboard/learn/strategies', methods=['GET'])
# def strategies():
#   return "strategies"

# @app.route('/dashboard/learn/types', methods=['GET'])
# def types():
#   return "Types"


if __name__ == '__main__':
  app.run(debug=True)