class Config:
  def __init__(self):
    from configparser import ConfigParser as conf
    conf = conf()
    conf.read('config.ini')
    self.api, self.secret = conf['default'].values()

  def values(self):
    return [self.api, self.secret]