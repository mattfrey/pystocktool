# Stock Analyzer v0.0.1
#
# Uses 6 key technical factors and ties in Market News for sector and
# stock news to evaluate sentiment and value.
#
# Feb 13, 2019
#

# Test comment - delete after

class SA:

    @staticmethod
    def PE_Eval(eps, pe, sector):
        if not eps > 0:
            return 0
        sectorAvg = sum(sector) / len(sector)
        perfRatio = round((pe / sectorAvg), 2)
        val = 0

        if perfRatio > 1:
            val = 1
        elif perfRatio <= 1 and perfRatio > 0.5:
            val = 0.75
        elif perfRatio < 0.5 and perfRatio > 0.25:
            val = 0.25
        else:
            val = 0

        return val

    @staticmethod
    def PB_Eval(pbr, assets_val=0):

        if pbr > 1:
            return 0
        return 1

    @staticmethod
    def PEG_Eval(peg):
        return 1 if peg < 1 else 0

    @staticmethod
    def ROE_Eval(roe):
        if roe < 0.17:
            return 0
        elif roe > 0.17 and roe < 0.2:
            return 0.5
        elif roe > 0.2 and roe < 0.25:
            return 0.75
        else:
            return 1

    @staticmethod
    def Debt_2_Equity_Eval(total_Debt, shareHolder_equity, DE=False):

        debt_to_equity = total_Debt / shareHolder_equity if not DE else DE
        if debt_to_equity < 1.5:
            return 1
        elif debt_to_equity < 2 and debt_to_equity > 1.5:
            return 0.5
        else:
            return 0

    @staticmethod
    def Liquidity_Ratio_Eval(liability, assets):
        lr = liability / assets
        return 1 if lr < 2 and lr > 1 else 0



